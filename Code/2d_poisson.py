from fenics import *
import numpy
import matplotlib.pyplot as plt
import dolfin
from dolfin import *
import mshr
from mshr import *

##################################################################
########################" Optimization options for the form compiler
###################################################################
parameters["form_compiler"]["cpp_optimize"] = True
parameters['form_compiler']['cpp_optimize_flags'] = '-O3 -ffast-math -march=native'
parameters["form_compiler"]["representation"] = "uflacs"

def change_mesh_size(input_mesh, x = 1, y = 1, z = 1):
    output_mesh = Mesh(input_mesh)
    output_mesh.coordinates()[:, 0] = x*input_mesh.coordinates()[:, 0]
    output_mesh.coordinates()[:, 1] = y*input_mesh.coordinates()[:, 1]
    #output_mesh.coordinates()[:, 2] = z*input_mesh.coordinates()[:, 2]
    return output_mesh
    
######################
#####################definition of the period epsilon and the matrix A 
####################

e = 0.025
epsilon = Constant(e)
A = Expression('pow((2 + cos( (2 * pi * x[0]) / epsilon)), -1)', degree = 1, epsilon = epsilon)
###################################
################################## Create RVE : subdomain
##################################
mesh_rve = change_mesh_size(UnitSquareMesh(20, 20), x = e, y = e)

#############################
############################# Sub domain for Dirichlet boundary condition
#############################
class DirichletBoundary(SubDomain):
    def inside(self, x, on_boundary):
        return bool((x[1] < DOLFIN_EPS or x[1] > (1.0 - DOLFIN_EPS)) \
                    and on_boundary)

#############################
############################## Sub domain for Periodic boundary condition
##############################


class PeriodicBoundary(SubDomain):

    def inside(self, x, on_boundary):
        # return True if on left or bottom boundary AND NOT on one of the two corners (0, epsilon) and (epsilon, 0)
        return bool((near(x[0], 0) or near(x[1], 0)) and 
              (not ((near(x[0], 0) and near(x[1], e)) or 
                    (near(x[0], e) and near(x[1], 0)))) and on_boundary)

    def map(self, x, y):
        if near(x[0], e) and near(x[1], e):
            y[0] = x[0] - e
            y[1] = x[1] - e
        elif near(x[0], e):
            y[0] = x[0] - e
            y[1] = x[1]
        else:   # near(x[1], e)
            y[0] = x[0]
            y[1] = x[1] - e

##########################
###########################   definition of total mesh
###########################

mesh = UnitSquareMesh(20, 20)

############################
############################ built function space and finit element 
############################
W = FunctionSpace(mesh_rve,"CG",1, constrained_domain=PeriodicBoundary()) # To introduce constraint $\int\ u\ d \Omega=0$. Is it needed?

############################
############################definition of the boundary condition 
############################

u0 = Constant(0.0)
dbc = DirichletBoundary()
bc0 = DirichletBC(W, u0, dbc)
#"on_boundary")

##### Collect boundary conditions
bcs = [bc0]



###################################
################################### resolution of the problem 
###################################

f = Constant(1.0)
v = TestFunction(W)
uh = Function(W)
a= inner(A*grad(uh), grad(v))*dx 
L = f*v*dx
dd= a- L
#w = Function(W)
solve(dd == 0 , uh , bcs)
file = File("periodic.pvd")
file << uh

plot(uh, interactive=True)

#(u_rve, c) = W.split(True)

# How to get homogenized material parameter `k` from `u_rve`?

###############

#mesh = UnitSquareMesh(20, 20) # Global mesh for homogenized solution
#V = FunctionSpace(mesh, "Lagrange", 1)

#f = Constant(1.0)
#bc = DirichletBC(V, Constant(0.0), "on_boundary")
#u = TrialFunction(V)
#v = TestFunction(V)

#k = # Homogenized material parameter
#a = inner(k*grad(u), grad(v))*dx
#L = f*v*dx

#u = Function(V)
#solve(a == L, u, bc)