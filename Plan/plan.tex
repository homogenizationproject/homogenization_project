% Template for a project plan
% ---------------------------
% Anders Logg, logg@math.chalmers.se
% 2003-01-30
% 2004-01-15
%
% Karin Kraft, kakr@math.chalmers.se
% 2005-01-12
%
% This file is intented for use with the LaTeX text formatting
% system. When you work with LaTeX, you write a '.tex' file and then
% use the command 'latex' to generate your document. By using LaTeX
% you will be able to create professional-looking documents.
%
% Instructions for usage:
%
%   1. Copy this file to a suitable directory.
%   
%      I suggest that you create a directory called 'projectplan'
%      where you put this file.
%
%   2. Compile the file using the latex command:
%
%        latex plan.tex
%
%      This will generate some output, and a file called 'plan.dvi'
%      will be created.
%
%   3. Now look at the resulting document using the command
%
%        xdvi plan.dvi &
%
%      This will start the program xdvi to show your document.
%      Notice the use of '&' that will keep xdvi going in the
%      background as you continue to work on the document.
%
%   4. Open the document file 'plan.tex' in an editor, such as emacs:
%
%        emacs plan.tex &
%
%      You are probably already reading this text in an editor, so then
%      you don't need to open it again... Now edit the contents below to
%      match the description of your project. When you are done, you once
%      again give the command
%
%        latex plan.tex
%
%      to compile your document. This will update the contents of the xdvi
%      window. If nothing changes, you need to type SHIFT-r in xdvi to force
%      a redisplay of the contents.
%
%   5. When finally you are satisfied with your document, you need to generate
%      a '.ps' or '.pdf' file for the distribution of your document (or for
%      printing). Use the command
%
%        dvips -o plan.ps plan.dvi
%
%      to generate a '.ps' file, and then (optionally)
%
%        ps2pdf plan.ps plan.pdf
%
%      to generate a '.pdf' file. To print the '.ps' file, use the command
%
%        lpr plan.ps
%
%      To view the '.ps' file, use the command 'gv plan.ps', and to view the
%      '.pdf' file, use the command 'acroread plan.pdf'.
%
%   For more information about how to use LaTeX, go to the website
%
%      http://www.dd.chalmers.se/latex/

\documentclass[11pt]{article}

\usepackage{a4wide}
\usepackage{fancyhdr}
\usepackage[latin1]{inputenc}

\lhead{Project plan -- \today}
\rhead{Computational Mathematical Modeling}
\cfoot{}
\pagestyle{fancy}

\setlength{\parindent}{0pt}
\setlength{\parskip}{6pt}

\begin{document}

\begin{center}
	\Large \textbf{Numerical homogenization}
\end{center}

\section*{Students}

\begin{tabular}{lll}
	Asma Manai     & University of Pierre and Marie Curie \\
	Lukas Mosser & Imperial College London \\
	Ivan Yashchuk & Aalto University \\
	Proposal by: Axel Malqvist
\end{tabular}

\section*{Aim of project}
\begin{itemize}
    \item  Investigate the dependency of the error with regards to the periodicity, mesh size and finite element basis.
    \item  Determine the accuracy of the homogenized solution for more complex/natural computational domains
    \item  Study the influence of the frequency and the amplitude of the periodic data on the convergence of the iterative solver.
\end{itemize}

\section*{Model description}
Many natural and synthetic materials consist of multiple components that have random and often periodic spatial organization of the microstructures.
Typical examples for these materials are sedimentary rocks made of granular components with interstitial pore space where flow of fluids occurs in the pore space between grains or fibrous materials that exhibit anisotropy in material strength according to the microscopic arrangement of fibers. 
Macroscopic properties that determine the material behavior of these random periodic structures can be obtained by experimental methods and compared
to numerical simulations. On a macroscopic scale solving the local fine-scale problem may be computationally too expensive and therefore a continuum
approach must be used to solve for the reactant forces or effective behavior at a large scale. To do so, it is necessary to determine effective properties acquired
at the micro-scale and incorporate these into the macro-scale simulation. The strong spatial variation in these effective properties across many orders of 
magnitude pose a challenge to traditional numerical methods. Homogenization methods alleviate these deficits by incorporating an explicit treatment of 
the small scale variation in the continuum problem.

To investigate the theoretical strengths, limitations and requirements of homogenization techniques, we will investigate a number of simple models. 
By solving equation \ref{equ:simple} we will investigate in one dimension:
\begin{enumerate}
	\item Convergence rate dependency on mesh size, finite element basis and the parameter $\epsilon$
	\item Implementation of periodic boundary conditions for homogenization techniques
\end{enumerate}

Based on the effective diffusion coefficient of materials we will develop a solution to two and three dimensional diffusion problem and compute the effective diffusion coefficient for the homogenized problem using FEniCS.

We are going to use elements with oscillating finite element basis.

\section*{Differential equation}

The differential equation is given by:
\begin{eqnarray*}\label{equ:simple}
        - \nabla \cdot A \nabla u  &=& f, \\
        A(\vec{x}) &=& \left(2 + \cos \left(\frac{2 \pi \vec{x}}{\epsilon} \right)\right)^{-1}, \\
        f(\vec{x}) &=& 1.
\end{eqnarray*}
with homogeneous Dirichlet boundary conditions


\section*{Challenges}

\begin{itemize}
\item Implementation of periodic boundary conditions in FEniCS

\item Treatment of boundaries the edge of the structure 

\item Implementation of  oscillating finite element basis in FEniCS

\item The theory around homogenization may pose a significant challenge in terms of transfering the theory to the actual implementation and investigation of the results e.g. convergence rates, mesh dependency

\end{itemize}

\section*{Expected outcome}

\begin{itemize}
	
	\item Convergence results of $u_\epsilon$ to $u$ in the Sobolev space $H_0^1(\Omega)$
	
	\item Convergence results of the iterative solvers for different conductivity $A$
	
	\item Plots of the solution for different computational domains
	
\end{itemize}

\section*{Motivation}

Effective properties play an important role in a number of material properties that are of interest to our team-members.
Fatigue may be dominated by the small-scale heterogeneity and variability of ferritic materials such as steel and new 
improved techniques to incorporate the variation in effective properties may lead to better predictive models for this material failure phenomenon.

For fluid flow in porous media the computation of effective diffusion coefficients and/or permeability of the material is 
an essential key property in the uncertainty quantification of of many natural porous media and play a role in the effective recovery of 
geothermal energy, carbon capture and sequestration as well as recovery of hydrocarbons from subsurface reservoirs. Homogenization techniques
may allow better predictive behavior for the natural and synthetic porous media and allow us to understand the implications of the small-scale 
variability on the large scale flow behavior.

\end{document}
